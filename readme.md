# IMC ALREADY FIXED THIS #
if your `131782_001001.ipf` is about 250MB

-----------------------

### Below was the old description ###

-----------------------

# **ALL I DO IS EXTRACT AND REPACK FROM IMC's DATA, NOTHING MORE, NOTHING LESS.** #

*Those files are in `12166_001001.ipf` before*

*** ***

Download & Install

Goto [Downloads](https://bitbucket.org/iDreamz/itoscosfix/downloads) tab and just download the file and put it into `<treeofsavior>/patch/` folder

**Please make sure that filename is `🚀WhyIDidIMCJob-costumefixes.ipf`**

`<treeofsavior>` = Game's directory from steam, usually `/steam/steamapps/common/treeofsavior`

*** ***

Item List

* Hat
    * Major Spade Hat
    * Diamond Magician Hat
    * Queen Diamnd's Tiara
    * Sailor Heart Hat
    * Clover Feather Hat
* Hairstyle
    * Quiff Style (Male)
    * Long-Wave Perm (Female)
* Costume
    * Trump Swordman (Male, Female)
    * Trump Wizard (Male, Female)
    * Trump Archer (Male, Female)
    * Trump Cleric (Male, Female)

*** ***

![Image](https://cdn.discordapp.com/attachments/176063360326959104/212513440072335361/WhyDidIDoIMCJob.jpg)

*** ***

Thanks

 * Excrulon, TehSeph, MrJul for guiding me an efficient way to pack IPF file
 * TwoLaid for efficient IPF tool
 * IPF Suite's team for wasting my time with your tool
 * Exec from Aura project for IPF Browser
 * Spl3en for IPFUnpacker (which broke .dds files)
 * IMC Games, for broke my client via your stupid Patcher and copypasta response, really thanks.